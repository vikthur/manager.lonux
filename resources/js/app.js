// import Toasted from 'vue-toasted';

import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css'

import lightBoxCSS from 'vue-image-lightbox/dist/vue-image-lightbox.min.css'

// import algoliasearch from 'algoliasearch/lite';
// window.algoliasearch = algoliasearch;

// import InstantSearch from 'vue-instantsearch';

import SweetModal from 'sweet-modal-vue/src/plugin.js'

import VueLazyLoad from 'vue-lazyload'

require('./bootstrap')

window.Vue = require('vue')

Vue.use(VueToast)
Vue.use(SweetModal)
Vue.use(VueLazyLoad)
Vue.use(lightBoxCSS)
    // Vue.use(InstantSearch)

// global mixins
Vue.mixin({
    data() {
        return {
            pathOrigin: window.location.origin,
            loader: {
                color: '#fd961a',
                loading: false
            }
        }
    }

})

// eventbus
export const lonuxbus = new Vue()

Vue.component('manager-login-component', require('./components/manager/auth/managerLoginComponent.vue').default)
Vue.component('m-sidenav', require('./components/manager/shared/sidenavComponent').default)
Vue.component('m-topnav', require('./components/manager/shared/TopnavComponent').default)
Vue.component('manager-home', require('./components/manager/home.vue').default)
Vue.component('manager-new-inventory', require('./components/manager/newInventory.vue').default)
Vue.component('manager-manage-inventory', require('./components/manager/manageInventory.vue').default)
Vue.component('list-inventory', require('./components/manager/listInventory.vue').default)
Vue.component('manager-settings', require('./components/manager/settings.vue').default)
Vue.component('manager-bookings', require('./components/manager/bookings.vue').default)
Vue.component('show-item', require('./components/manager/showItem.vue').default)
Vue.component('edit-item', require('./components/manager/editItem.vue').default)
Vue.component('show-booked-item', require('./components/manager/showBookingItem.vue').default)

// service
Vue.component('service-manager-home', require('./components/manager/service/ServiceManagerHome.vue').default)
Vue.component('manager-new-service-inventory', require('./components/manager/service/newServiceInventory.vue').default)
Vue.component('manager-manage-services', require('./components/manager/service/ManageServices.vue').default)
Vue.component('show-service', require('./components/manager/service/ShowService.vue').default)
Vue.component('edit-service', require('./components/manager/service/EditService.vue').default)

/**
 *
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
})