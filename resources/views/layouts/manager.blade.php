<!DOCTYPE html>
<html style="background-color: #eee">
<head>
	<meta charset="UTF-8">

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>lonux - @yield('title')</title>

	{{-- favicon --}}
	<link rel="shortcut icon" href="{{asset('img/lonux-icon.svg')}}" type="image/x-icon">

	
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	
	{{-- <link rel="stylesheet" href="css/style"> --}}

	<!-- icons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

	{{-- //init user details for vue usage --}}
	<script>
		window.lonux = {
			
			'csrf_token': "{{ csrf_token() }}",
			"userId": "{{ lonuxId() }}"
			
		}
	</script>
</head>

<body style="background-color: #eee">
	<div id="app">
		<div class="container-fluid dashboard bg-light">
			<m-sidenav></m-sidenav>

			<div class="dashboard-content">
				<div class="row justify-content-end">
					<div class="col-md-9 position-relative">
						<m-topnav></m-topnav>
						@yield('content')
					</div>
				</div>
			</div>
	</div>

	</div>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/push-notification.js')}}"></script>

<script>
	initSW()
</script>
</body>
</html>