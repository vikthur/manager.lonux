<!DOCTYPE html>
<html style="background-color: #eee">
<head>
	<meta charset="UTF-8">

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>lonux - @yield('title')</title>

	{{-- favicon --}}
	<link rel="shortcut icon" href="{{asset('img/lonux-icon.svg')}}" type="image/x-icon">

	
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	
	{{-- <link rel="stylesheet" href="css/style"> --}}

	<!-- icons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

	{{-- //init user details for vue usage --}}
	<script>
		window.lonux = {
			"has_setup": "{{ lonuxUser()->has_setup }}",
			"company": "{{ lonuxUser()->company ? lonuxUser()->company->name : null }}",
			"accType": "{{ lonuxUser()->accType ? lonuxUser()->accType->name : null }}",
			"name": "{{ lonuxName() }}",
			"shopsCount": "{{ count(lonuxUser()->shops) }}",
			'csrf_token': "{{ csrf_token() }}",
			'userId': "{{lonuxId()}}",
			'rider' : "{{ lonuxUser()->rider ? lonuxUser()->rider : null }}",
		}
	</script>
</head>
<body style="background-color: #eee">
	<div id="app">

		@yield('content')

	</div>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>