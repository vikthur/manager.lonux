
<!DOCTYPE html>
<html lang="en" style="height: 100%">
<head>
	<meta charset="UTF-8">

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<title>lonux - @yield('title')</title>

	{{-- favicon --}}
	<link rel="shortcut icon" href="{{asset('img/lonux-icon.svg')}}" type="image/x-icon">

	
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	
	<!-- icons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

</head>
<body class="bg-white">
	<div  id="app">
	<div>
		{{-- @include('layouts.misc.bookings') --}}
	</div>	
		<nav class="navbar navbar-expand-sm bg-white nav-lonux-light">
		
			<div class="container">
					<a href="/" class="navbar-brand">
					<img src="{{asset('img/lonux-logo-01.svg')}}" alt=""  class="img-brand">
					</a>
					<button class="navbar-lonux-toggler ml-auto" id="nav-toggle" type="button" >
						<span class="navbar-toggler-icon">
							<svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M10 13H17M1 1H17H1ZM1 7H17H1Z"  stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>
						</span>
					</button>
					<div class="nav-content" id="nav-content">
						<button class="close">
							<i class="material-icons text-dark">close</i>
						</button>
						<div class="nav-menu">
							<ul class="nav flex-column nav-gen">
								@guest

								<li class="nav-item">
									<a class="nav-link lonux-link" href="/m/login">Manager Portal</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="#">About lonux</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="#">Terms of service</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="#">Get help</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="/login">sign in</a>
								</li>
								<!-- <li class="nav-item">
									<a class="nav-link lonux-link" href="/auth/phone">sign up</a>
								</li> -->
								@else
								<li class="nav-item">
									<a class="nav-link lonux-link" href="/home">Dashboard</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="/m/login">Manager Portal</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="/home">Account setting</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="#">Terms of service</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="#">Get help</a>
								</li>
								@endguest
							</ul>

							<ul class="nav nav-social">
								<li class="nav-item">
									<a class="nav-link lonux-link" href="/login">
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M12 0.480011C5.63758 0.480011 0.47998 5.63761 0.47998 12C0.47998 18.3624 5.63758 23.52 12 23.52C18.3624 23.52 23.52 18.3624 23.52 12C23.52 5.63761 18.3624 0.480011 12 0.480011ZM16.686 9.91681C16.6908 10.0152 16.692 10.1136 16.692 10.2096C16.692 13.2096 14.4108 16.6668 10.2372 16.6668C9.00393 16.6689 7.79634 16.3147 6.75958 15.6468C6.93598 15.6684 7.11718 15.6768 7.30078 15.6768C8.36398 15.6768 9.34198 15.3156 10.1184 14.706C9.64524 14.6967 9.18679 14.54 8.80691 14.2578C8.42704 13.9756 8.14468 13.5819 7.99918 13.1316C8.33897 13.1962 8.689 13.1827 9.02278 13.092C8.50924 12.9882 8.04742 12.7099 7.71563 12.3044C7.38384 11.8989 7.20251 11.3911 7.20238 10.8672V10.8396C7.50838 11.0088 7.85878 11.112 8.23078 11.124C7.74934 10.8035 7.40852 10.3112 7.27803 9.74777C7.14755 9.18432 7.23724 8.59231 7.52878 8.09281C8.09873 8.79361 8.80949 9.36691 9.61506 9.77558C10.4206 10.1843 11.303 10.4192 12.2052 10.4652C12.0905 9.97838 12.1399 9.4673 12.3456 9.01142C12.5513 8.55553 12.9019 8.1804 13.3429 7.94433C13.7838 7.70826 14.2904 7.62448 14.7839 7.70602C15.2773 7.78756 15.73 8.02984 16.0716 8.39521C16.5794 8.29474 17.0664 8.10848 17.5116 7.84441C17.3423 8.37017 16.988 8.81662 16.5144 9.10081C16.9642 9.04663 17.4034 8.9257 17.8176 8.74201C17.5134 9.19788 17.1301 9.59574 16.686 9.91681Z"/>
										</svg>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link lonux-link" href="/login">
										<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M13.0011 2.16882C7.01891 2.16882 2.16882 7.01891 2.16882 13.0011C2.16882 18.4069 6.12949 22.8876 11.3089 23.7033V16.133H8.55724V13.0011H11.3089V10.6145C11.3089 7.89749 12.9263 6.39924 15.3996 6.39924C16.5847 6.39924 17.8262 6.61049 17.8262 6.61049V9.27441H16.4569C15.1136 9.27441 14.6932 10.1107 14.6932 10.9677V12.9989H17.6952L17.2152 16.1308H14.6932V23.7012C19.8727 22.8897 23.8333 18.408 23.8333 13.0011C23.8333 7.01891 18.9832 2.16882 13.0011 2.16882Z" />
										</svg>

									</a>
								</li>
							</ul>
						</div>
					</div>
				
			</div>
		</nav>

		@yield('content')	
		
	</div>




	


	<script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{asset('bootstrap/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/app.js')}}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCxtgFvO4kRmVcvb31g_PHm0HYIf2894ms"></script>



	<script>

		$('document').ready(()=>{

			$('#nav-toggle').on('click', ()=>{
				$('#nav-content').addClass('show');
				setTimeout(() => {
					$('.nav-menu').fadeIn(300);
				}, 300);
			})
			
			$('#nav-content .close').on('click', ()=>{

				$('.nav-menu').fadeOut(300);
				setTimeout(() => {
					$('#nav-content').removeClass('show');
					
				}, 300);
			})
		})

	</script>
</body>
</html>