@extends('layouts.manager')

@section('title')

manager home

@endsection

@section('content')

    <manager-home :shop="{{$shop}}"></manager-home>
@endsection