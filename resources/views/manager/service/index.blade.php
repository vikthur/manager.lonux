@extends('layouts.manager')

@section('title')

manager home

@endsection

@section('content')
    <service-manager-home :shop="{{$shop}}"></service-manager-home>
@endsection