<!DOCTYPE html>
<html lang="en" style="height: 100%">
<head>
	<meta charset="UTF-8">

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Manager login</title>

	
	<link rel="shortcut icon" href="{{asset('img/lonux-icon.svg')}}" type="image/x-icon">

	
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	
	<!-- icons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body class="bg-light">
	<div  id="app">
		<nav class="navbar navbar-expand-sm bg-light ">
			<div class="container">
					<a href="/" class="navbar-brand">
					<img src="{{asset('img/lonux-logo-01.svg')}}" alt=""  class="img-brand">
					</a>
				
			</div>
		</nav>
        <manager-login-component></manager-login-component>
	</div>
	
	<!-- {{-- scripts --}} -->
	<script src="{{asset('js/jquery.js')}}"></script>
	<script src="{{asset('bootstrap/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/app.js')}}"></script>
</body>


</html>