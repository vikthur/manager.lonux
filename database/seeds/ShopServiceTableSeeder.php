<?php

use Illuminate\Database\Seeder;

class ShopServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_services')->insert(
        	[
	        	[
	            'name' => 'Open Sales',
	        	],
	        	[
	            'name' => 'Services Oriented',
	        	],
        	]
    	);
    }
}
