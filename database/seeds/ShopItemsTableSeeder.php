<?php

use Illuminate\Database\Seeder;

class ShopItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Lonux\ShopItems::class, 10000)->create();
    }
}
