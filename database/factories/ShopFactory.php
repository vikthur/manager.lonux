<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Lonux\Shop;
use Faker\Generator as Faker;

$factory->define(Shop::class, function (Faker $faker) {
    return [
        'shop_name' => $faker->company,
        'shop_address' => $faker->address,
        'shop_service_id' => $faker->numberBetween(1,2),
        'shop_type_ids' => implode(',',$faker->randomElements([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], 7)),
        'shop_sells_in' => $faker->randomElement(['wholesale','retail']),
        'shop_work_days' => implode(',',$faker->randomElements(['Mondays','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'], 6)),
        'shop_open_at' => $faker->randomElement(['5AM','6AM','7AM','8AM','9AM']),
        'shop_close_at' => $faker->randomElement(['7PM','8PM','9PM','10PM','11PM','12AM']),
        'lat' => $faker->latitude($min = -90, $max = 90),
        'lng' => $faker->longitude($min = -180, $max = 180),
        'email' => $faker->unique()->companyEmail,
        'is_up' => $faker->numberBetween(0,1),
        'company_id' => $faker->numberBetween(1,200),
        'user_id' => $faker->numberBetween(1,200),
        'key' => $faker->unique()->uuid,
    ];
});
