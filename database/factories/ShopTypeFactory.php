<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Lonux\ShopType;
use Faker\Generator as Faker;

$factory->define(ShopType::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(["Provisions","Cosmetics","Pharamacy","Fabrics","Boutique","Electronics","Telecomms","Fruits","Snacks","Restaurnt","Electronic Repairs","Cubler","Gambling","Telecoms Repair","Item One","Item Two","Item Three","Item Four","Item Five","Item six"]),
        'shop_service_id' => $faker->numberBetween(1,2),
    ];
});
