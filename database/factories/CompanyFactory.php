<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Lonux\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->company,
        'email' => $faker->unique()->companyEmail,
        'hq_address' => $faker->address,
        'lat' => $faker->latitude($min = -90, $max = 90),
        'lng' => $faker->longitude($min = -180, $max = 180),
        'nafdac_reg_no' => 'NF'.$faker->ean8,
        'user_id' => $faker->unique()->numberBetween(1,200),
        'cac_reg_no' => 'CAC'.$faker->isbn10,
        'desc' => $faker->text,
        'key' => $faker->unique()->uuid,
    ];
});
