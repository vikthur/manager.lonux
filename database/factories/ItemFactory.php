<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Lonux\Item;

$factory->define(Item::class, function (Faker $faker) {
    $faker->addProvider(new \FakerRestaurant\Provider\en_US\Restaurant($faker));
    return [
        'item_name' => $faker->randomElement([$faker->foodName(),$faker->beverageName(),$faker->dairyName(),$faker->vegetableName(),$faker->fruitName(),$faker->meatName(),$faker->sauceName()]),
        'item_cost' => $faker->numberBetween(100, 1000),
        'upc' => $faker->isbn13(),
        'item_category_id' => $faker->numberBetween(1, 5),
    ];
});