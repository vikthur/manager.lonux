<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shop_name');
            $table->string('shop_address');
            $table->integer('shop_service_id');
            $table->text('shop_type_ids');
            $table->string('shop_sells_in');
            $table->text('shop_work_days');
            $table->string('shop_open_at');
            $table->string('shop_close_at');
            $table->float('lat', 10, 8);
            $table->float('lng', 11, 8);
            $table->string('contact_code')->nullable();
            $table->string('shop_image')->nullable();
            $table->string('email')->nullable();
            $table->boolean('is_up')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
