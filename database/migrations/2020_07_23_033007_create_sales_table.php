<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->integer('shop_id');
            $table->integer('user_id')->nullable();
            $table->integer('shop_items_id');
            $table->enum('bought_in',['retail', 'wholesale']);
            $table->enum('buying_channel', ['Booking', 'Order']);
            $table->enum('pickup_mode', ['Customer Pickup', 'Delivery']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
