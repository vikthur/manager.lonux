<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('amount');
            $table->integer('available_in_stock');
            $table->integer('pieces_available')->nullable();
            $table->integer('sold')->default(0);
            $table->enum('sold_in',['wholesale','retail','both'])->default('retail');
            $table->integer('shop_id');
            $table->string('upc')->nullable();
            $table->json('item_history')->nullable();
            $table->integer('item_category_id');
            $table->text('desc')->nullable();
            $table->text('images')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_items');
    }
}
