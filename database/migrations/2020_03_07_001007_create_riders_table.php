<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->uuid('key');
            $table->string('driver_license');
            $table->string('vehicle_name');
            $table->string('vehicle_number');
            $table->string('vehicle_type');
            $table->date('date_of_birth');
            $table->string('rider_picture');
            $table->string('vehicle_picture');
            $table->boolean('approved')->default(0);
            $table->boolean('is_open_to_delivery')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riders');
    }
}
