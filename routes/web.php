<?php

Route::get('/', "manager\HomeController@index")->middleware('is_manager');

// Auth::routes();

// Route::get('/login', 'manager\Auth\LoginController@showLoginForm')->name('mLogin');

Route::get('/m/login', 'manager\Auth\LoginController@showLoginForm')->name('mLogin');
Route::post('/m/login', 'manager\Auth\LoginController@login');
Route::post('/m/logout', 'manager\Auth\LoginController@logout');
//shop admins/managers

// shop admins/managers
Route::group(['middleware' => 'is_manager'], function () {
	Route::get('/m/manage-inventory', "manager\HomeController@manageInventory");
	Route::get('/m/new-inventory', "manager\HomeController@newInventory");
	Route::get('/m/settings', "manager\HomeController@settings");
	Route::get('/m', "manager\HomeController@index");

	// store inventory
	Route::post('/m/add_inventory', 'manager\InventoryController@storeInventory');
	Route::get('/m/get_items_categories', 'manager\InventoryController@getItemsCategory');
	Route::get('/m/get_latest_inventory', 'manager\InventoryController@getLatestInventory');
	Route::get('/m/service/all', 'manager\ServiceController@showAll');
	Route::get('/m/service/unavailable/{id}', 'manager\ServiceController@unavailable');
	Route::resource('/m/service', 'manager\ServiceController');

	// view/manage inventory
	Route::get('/m/get_shop_items_category', 'manager\InventoryController@getShopItemsCategory');
	Route::get('/m/manage-inventory/{id}/{name}', "manager\HomeController@getCategoryItems");
	Route::get('/m/get_category_items/{id}', 'manager\InventoryController@getCategoryItems');
	Route::get('/m/get_inventory-item/{id}', 'manager\InventoryController@getInventoryItem');
	Route::get('/m/show-inventory-item/{id}/{name}', 'manager\HomeController@showItem');
	Route::post('m/get_item_by_upc', 'manager\InventoryController@getItemByUPC');
	Route::get('/m/bookings', function () {
		return view('manager.bookings');
	});

	// update items
	Route::post('/m/item_is_out_of_stock', 'manager\InventoryController@itemIsOutOfStock');
	Route::post('m/update_inventory', 'manager\InventoryController@updateInventory');
	Route::get('/m/edit_item/{id}/{name}', 'manager\HomeController@editItem');
	Route::post('/m/delete_item', 'manager\InventoryController@deleteItem');

	// bookings
	Route::get('/m/get_bookings', 'manager\InventoryController@getBookings');
	Route::get('/m/show-booking/{id}/{name}', 'manager\HomeController@showBooking');
	Route::get('/m/get_booked_item/{id}', 'manager\InventoryController@getBookedItem');
	Route::post('/m/item_was_picked', 'manager\InventoryController@itemWasPicked');
	Route::post('/m/customer_didnt_come', 'manager\InventoryController@customerDidntCome');
	Route::post('/m/search_booking', 'manager\InventoryController@searchBooking');
	Route::post('/file/change_this_image', 'manager\InventoryController@changeThisImage');
	Route::post('file/add_new_image', 'manager\InventoryController@addNewImage');
	Route::post('file/delete_image', 'manager\InventoryController@deleteImage');
	// push notification
	Route::post('/m/subscribe_manager', 'manager\HomeController@subscribeManager');

	// shop 
	Route::post('/m/update_shop_status/{id}', 'manager\HomeController@updateShopStatus');
	Route::get('/m/get_shop_status/{id}', 'manager\HomeController@getShopStatus');
	Route::post('/m/shop/settings/time', 'manager\HomeController@changeShopTime');
});



Broadcast::routes();

// Route::get('/user/{vue_capture?}', function () {
//     return view('user.dashboard');
// })->where('vue_capture', '[\/\w\.-]*')->middleware('auth');

// Route::get('/{vue_capture?}', function () {
//     return view('pages.home');
// })->where('vue_capture', '[\/\w\.-]*')->middleware('auth');
