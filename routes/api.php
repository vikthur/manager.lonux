<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1/user')->middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1/user')->group(function(){
  	// authentication
  	Route::post('/auth/phone', 'PhoneNumberVerification@phoneVerification');

	Route::get('/auth/phone/{phone}', 'PhoneNumberVerification@changeNumber');

	Route::post('/login', 'api\AuthController@login');

	Route::post('/register', 'api\AuthController@register');

	Route::post('/password-reset', 'PhoneNumberVerification@changePassword');

	// search
	Route::post('/get_item_shops', 'SearchController@ApiSearch');

	Route::post('/save_user_activity', 'UserController@saveUserActivity');

	Route::post('/save_shop_search', 'UserController@saveShopSearchAppearance');

	Route::get('/get_item_data/{shop_Id}/{item}', 'SearchController@getItemData');

	Route::get('/get_item_suggestions/{query}', 'SearchController@getItemSuggestions');

	Route::get('/get_related_items/{shop_id}/{item}', 'SearchController@getRelatedItems');

	// shop rating
	Route::post('/save_rating', 'MiscController@saveRating');

	Route::post('/test', 'SearchController@testDist');

	// bookings
	Route::resource('booking', 'BookingController');

	// user
	Route::group(['middleware' => 'auth:sanctum'], function(){
		Route::get('/get_stats', 'UserController@getStats');
		Route::get('/get_bookings', 'UserController@getBookings');
		Route::get('/get_activities', 'UserController@getActivities');
	});

});

Route::prefix('v1/m')->group(function(){
  Route::post('/mlogin', 'api\AuthController@mLogin');
  Route::post('/upc_check', 'manager\InventoryController@getItemByUPC')->middleware('auth:sanctum');
  Route::post('inventory_store', 'manager\InventoryController@storeInventory')->middleware('auth:sanctum');
});

Route::get("test_json", function(){
  $array = [];
  $arr = [
    "val1" => "this is val 1",
    "val2" => "this is val 2",
    "val3" => 3,
  ];

  array_push($array, $arr);

  $arr2 = [
    "val11" => "this is val 11",
    "val21" => "this is val 21",
    "val31" => 31,
  ];

  $json = json_encode($array);


  $j_arr = json_decode($json);
  // dd($j_arr);
  array_push($j_arr, $arr2);

  // dd($j_arr);

  dd(json_encode($j_arr));
});