<?php

namespace Lonux;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'shop_managers';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company()
    {
        return $this->hasOne('Lonux\Company');
    }

    public function accType()
    {
        return $this->hasOne('Lonux\AccountTypes', 'id', 'account_type_id');
    }

    public function shops()
    {
        return $this->hasMany('Lonux\Shop');
    }

    public function rider()
    {
        return $this->hasOne('Lonux\Rider');
    }

    public function purchases()
    {
        return $this->hasMany('Lonux\Sale');
    }

    public function bookings()
    {
        return $this->hasMany('Lonux\Booking');
    }

    public function activities()
    {
        return $this->hasMany('Lonux\UserActivity');
    }
}
