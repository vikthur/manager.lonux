<?php

namespace Lonux;

use Illuminate\Database\Eloquent\SoftDeletes;

class ShopItems extends Model
{
    use SoftDeletes;

    public function defaultImage()
    {
        return 'https://res.cloudinary.com/daemoncoder/image/upload/v1623020424/lonux-shop-medium_z2txor.jpg';
    }

    public function category()
    {
        return $this->belongsTo('Lonux\ItemCategory', 'item_category_id', 'id');
    }

    public function sales()
    {
        return $this->hasMany('Lonux\Sale');
    }

    public function salesCount()
    {
        $salesArr = $this->sales->toArray();
        // dd($salesArr);
        if (count($salesArr)) {
            $totalSales = 0;
            foreach ($salesArr as $key => $value) {
                $totalSales += $value['quantity'];
            }
            return $totalSales;
        }

        return 0;
    }
}
