<?php

namespace Lonux;

class Company extends Model
{

    public function user()
    {
    	return $this->belongsTo('Lonux\User');
    }
    public function shops()
    {
        return $this->hasMany('Lonux\Shop');
    }

    public function managers()
    {
        return $this->hasMany('Lonux\ShopManager');
    }

    public function companyData()
    {
        $shops = $this->shops;
        
        $totalSales = 0;
        $totalManagers = 0;
        $totalInventory = 0;

        foreach ($shops as $key => $shop) {
            $sales = $shop->sales;
            foreach ($sales as $key => $sale) {
                $totalSales = $totalSales + $sale->quantity;
            }

            $totalManagers = $totalManagers + count($shop->managers);
            $totalInventory = $totalInventory + count($shop->items);
        }
        $data = [
            "totalSales" => $totalSales,
            "totalManagers" => $totalManagers,
            "totalInventory" => $totalInventory,
            "totalShops" => count($this->shops),
            "data" => $this
        ];

        return $data;
    }
}
