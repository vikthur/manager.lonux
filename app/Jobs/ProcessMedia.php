<?php

namespace Lonux\Jobs;

use Cloudder;
use Lonux\ShopItems;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessMedia implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $images;
    protected $file;
    protected $shopItem;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ShopItems $shopItem)
    {
        $this->shopItem = $shopItem;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shopItem = $this->shopItem;
        $images = $shopItem->images();
        $arr = [];
        print_r($images);
        $slug = Str::slug(substr($shopItem->name, 0, 10));

        foreach ($images as $key => $image) {
            $dir = 'shop_items_images/'.$slug.'_'.time();
            $uploadedFileUrl = Cloudder::upload($image, $dir)->getResult()["secure_url"];
            array_push($arr, $uploadedFileUrl);
        }

        // if ($images->image1) {
        //     $dir = 'shop_items_images/'.$slug.'_'.time();
        //     $uploadedFileUrl = Cloudder::upload_large($images->image1, $dir)->getResult()["secure_url"];
        //     array_push($arr, $uploadedFileUrl);
        // }
        // if ($images->image2) {
        //     $dir = 'shop_items_images/'.$slug.'_'.time();
        //     $uploadedFileUrl = Cloudder::upload($images->image1, $dir)->getResult()["secure_url"];
        //     array_push($arr, $uploadedFileUrl);
        // }
        // if ($images->image3) {
        //     $dir = 'shop_items_images/'.$slug.'_'.time();
        //     $uploadedFileUrl = Cloudder::upload($images->image1, $dir)->getResult()["secure_url"];
        //     array_push($arr, $uploadedFileUrl);
        // }
        // if ($images->image4) {
        //     $dir = 'shop_items_images/'.$slug.'_'.time();
        //     $uploadedFileUrl = Cloudder::upload($images->image1, $dir)->getResult()["secure_url"];
        //     array_push($arr, $uploadedFileUrl);
        // }
        // if ($images->image5) {
        //     $dir = 'shop_items_images/'.$slug.'_'.time();
        //     $uploadedFileUrl = Cloudder::upload($images->image1, $dir)->getResult()["secure_url"];
        //     array_push($arr, $uploadedFileUrl);
        // }

        $shopItem->images = implode(',', $arr);

        print($shopItem->images);

        $shopItem->save();
    }
}
