<?php

namespace Lonux;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use Searchable;

    // remove on production
    public function searchableAs()
    {
        return config('scout.prefix').'dev';
    }

    public function category()
    {
        return $this->belongsTo('Lonux\ItemCategory', 'item_category_id', 'id');
    }
}
