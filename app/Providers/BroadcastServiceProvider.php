<?php

namespace Lonux\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Broadcast::channel('Lonux.ShopManager.{managerId}', function ($manager, $managerId) {
            return (int) $manager->id === (int) $managerId;
        }, ['guards' => ['manager']]);

        require base_path('routes/channels.php');
    }
}
