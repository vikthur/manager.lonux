<?php

namespace Lonux;

class ItemCategory extends Model
{
    public function items()
    {
    	return $this->hasMany('Lonux\ShopItems');
    }
}
