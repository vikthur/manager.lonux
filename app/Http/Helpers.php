<?php
require __DIR__.'/../../vendor/laravel/framework/src/Illuminate/Support/Facades/Auth.php';

function lonuxUser()
{
	if (Auth::check()) {
		return Auth::user();
	}
	return null;
}

function lonuxName()
{
	if(!is_null(lonuxUser()))
	{
		return lonuxUser()->name;
	}elseif(!is_null(lonuxManager()))
	{
		return lonuxManager()->name;
	}else{
		return null;
	}

}

function lonuxId()
{
	if(!is_null(lonuxUser()))
	{
		return lonuxUser()->id;
	}elseif(!is_null(lonuxManager()))
	{
		return lonuxManager()->id;
	}else{
		return null;
	}
	
}

function lonuxManager()
{
	if(Auth::guard('manager')->check())
	{
		return Auth::guard('manager')->user() ;
	}

	return null;
}

function managerShopKey () 
{

	return lonuxManager()->shop_key;

}

function shopId () 
{
	return lonuxManager()->shop()->id;
}