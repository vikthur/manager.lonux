<?php

namespace Lonux\Http\Controllers;

use Lonux\Company;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;

class CompanyController extends Controller
{
    use SendResponse;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = lonuxUser()->company;
        $data = $company->companyData();
        return $this->send_response(true, 'shop created successfully', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $business = new Company();

        $business->user_id = lonuxId();
        $business->name = $request->name;
        $business->desc = $request->desc;
        $business->hq_address = $request->address;
        $business->lat = $request->lat;
        $business->lng = $request->lng;
        if ($request->cac) {
            $business->cac_reg_no = $request->cac;
        }
        if ($request->nafdac) {
            $business->nafdac_reg_no = $request->nafdac;
        }

        $email = str_replace(" ", "",ucwords($request->name));
        $email = $email."@lonux.com.ng";
        $email = $this->UniqueEmail($email);
        $business->email = $email;
        $business->key = (string)Str::uuid();

        $business->save();

        return $this->send_response(true, 'company created successfully');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->send_response(true, 'data retrieved successfully', lonuxUser()->company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = lonuxUser()->company;

        $company->name = $request->name;
        $company->hq_address = $request->hq_address;
        $company->desc = $request->desc;

        $company->save();

        return response("ok", 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function UniqueEmail($email){
        $company = Company::where("email",$email)->first();
        if (!is_null($company)) {
            $num = rand(10, 10000);
            $email = $email.$num."@lonux.com.ng";
            $this->UniqueEmail($email);
        }else{
            return $email;
        }
    }

    public function changeShopsTime(Request $request)
    {
        $shops = lonuxUser()->company->shops()->update([
            'shop_open_at' => $request->open_at,
            'shop_close_at' => $request->close_at,
        ]);

        return $this->send_response(true, "shops modified successfully");
    }
}
