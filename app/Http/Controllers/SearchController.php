<?php

namespace Lonux\Http\Controllers;

use Lonux\Shop;
use Lonux\ShopItems;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Illuminate\Support\Facades\DB;
use TeamPickr\DistanceMatrix\DistanceMatrix;
use TeamPickr\DistanceMatrix\Licenses\StandardLicense;

class SearchController extends Controller
{
    use SendResponse;

    public function getItemSuggestions($val)
    {
        $items = ShopItems::where('name', 'LIKE', $val.'%')->pluck('name')->toArray();
        
        if($items){
            $res = array_unique($items);
            $newArr = array_values($res);
            return $this->send_response(true, "suggestions retrieved successfully", $newArr);
        }else{
            return $this->send_response(true, "no items found");
        }   
    }
    
    public function searchPage(Request $request)
    {
        $item = $request->q;
        
        if(is_null($item))
        {
            return back();
        }
        
        return view('search', compact('item'));
    }
    
    public function getItemShops(Request $request)
    {
        $shopsIds = ShopItems::where('name', $request->item)->pluck('shop_id')->toArray();
        
        $shops = Shop::whereIn('id', $shopsIds)->get();
        
        foreach($shops as $shop){
            $shop->shop_rating = $shop->shopRating();
            $shop->image = $shop->image('card');
        }
        
        return $this->send_response(true, "shops retrieved successfully", $shops);
    }
    
    public function resultPage($key)
    {
        $shop = Shop::getShopByKey($key);
        
        return view('result', compact('shop'));
    }
    
    public function getRelatedItems($shopId, $item)
    {
        $item = ShopItems::where('name', $item)->first();

        if(is_null($item)){
            return $this->send_response(true, "no related items", []);
        };

        $catId = $item->item_category_id;
        $relatedItems = ShopItems::where('shop_id', $shopId)->where('item_category_id', $catId)->whereNotIn('name', [$item])->limit(10)->get();

        foreach ($relatedItems as $relatedItem) {
            $relatedItem->image = $relatedItem->image('card');
        }
        
        return $this->send_response(true, "related items retrieved", $relatedItems);
    }
    
    public function ApiSearch(Request $request)
    {
        $this->validate($request, [
            'item' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
            
        $shopsIds = ShopItems::where('name', $request->item)->pluck('shop_id')->toArray();
            
        $shops = Shop::whereIn('id', $shopsIds)->get()->toArray();
            
        $shopsWithDist = [];
        $unit = "K";
        foreach($shops as $key => $shop)
        {
            $lat1 = $request->lat; $lat2 = $shop['lat'];
            $lon1 = $request->lng; $lon2 = $shop['lng'];
                
            if (($lat1 == $lat2) && ($lon1 == $lon2)) {
                $shop->dist = 0;
            }
            else {
                    
                $theta = $lon1 - $lon2;
                    
                $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $unit = strtoupper($unit);
                    
                $shop_dist = $miles;
                    
                if ($unit == "K") {
                    $shop_dist = ($miles * 1.609344);
                } else if ($unit == "N") {
                    $shop_dist = ($miles * 0.8684);
                } else {
                    $shop_dist = $miles;
                }
                    
                $shop['dist'] = $shop_dist;
            }
                
            array_push($shopsWithDist, $shop);
                
        }
            
        $array = collect($shopsWithDist)->sortBy('dist')->toArray();
        
        $first_ten = [];
            
        foreach($array as $key => $arr){
            if(count($first_ten) < 10){
                $first_ten[] = $arr;
            }
        }
            
        $finalArray = [];
            
        $license = new StandardLicense(env('GOOGLE_MAPS_KEY'));
            
        foreach($first_ten as $key => $ft)
        {
            $response = DistanceMatrix::license($license)
            ->addOrigin($request->lat.','.$request->lng)
            ->addDestination($ft['lat'].','.$ft['lng'])
            ->request();
                
            // print_r($response);
                
            // I want to make the following but of API better,
            // as it looks horrible at the moment.
            $rows = $response->rows();
            $elements = $rows[0]->elements();
            $element = $elements[0];
                
            // if($element['status'] == 'ZERO_RESULTS'){
            //     $ft['walkTime'] = "UNKNOWN";
            // }
                
            // $distance = $element->distance();
            // $distanceText = $element->distanceText();
            // $duration = $element->duration();
            $durationText = $element->durationText();
            
            if(is_null($durationText)){
                $ft['driveTime'] = "UNKWOWN";
            }else{
                $ft['driveTime'] = $durationText;
            }
                
            array_push($finalArray, $ft);
        }
        
        return $this->send_response(true, "search successful", $finalArray);         
    }

    public function getItemData($shop_id, $item){
        $items = ShopItems::where('name', $item)->where('shop_id', $shop_id)->first();
        
        if(is_null($items))
        {
            return $this->send_response(false, "item not found", [], 404);
        }
        $items->image = $items->image('card');

        return $this->send_response(true, "item retrieved successfully", $items);
    }
            
    public function testDist(){
        $lat1 = 52.2165157; $lat2 = 52.3546274;
        $lon1 = 6.9437819; $lon2 = 4.8285838;

        $unit = 'K';
        
        $theta = $lon1 - $lon2;
        
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        
        $shop_dist = $miles;
        
        if ($unit == "K") {
            $shop_dist = ($miles * 1.609344);
        } else if ($unit == "N") {
            $shop_dist = ($miles * 0.8684);
        } else {
            $shop_dist = $miles;
        }

        dd($shop_dist);
    }
}
