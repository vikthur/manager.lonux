<?php

namespace Lonux\Http\Controllers\manager;

use Lonux\Item;
use Lonux\Sale;
use Lonux\Booking;
use Lonux\ShopItems;
use Lonux\ItemCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Lonux\Traits\SendResponse;
use JD\Cloudder\Facades\Cloudder;
use Lonux\Http\Controllers\Controller;
use Lonux\OfferedService;
use Throwable;

class InventoryController extends Controller
{
    use SendResponse;

    public $CloudinaryBaseUrl = 'https://res.cloudinary.com/daemoncoder/image/upload/';

    public function getItemsCategory()
    {
        $cats = ItemCategory::pluck('item')->toArray();

        return $this->send_response(true, "Items Category Retreved Successfully", $cats, 200);
    }

    public function storeInventory(Request $request)
    {
        $this->validate($request, [
            'item_category' => 'required',
            'item_sold_in' => 'required',
            'item_name' => 'required',
            'item_cost' => 'required',
            'item_stock' => 'required',
        ]);

        $shopId = lonuxUser()->shop()->id;

        $checkCategory = ItemCategory::where('shop_id', $shopId)->where('item', $request->item_category)->first();
        $cat = null;

        if (!is_null($checkCategory)) {
            $cat = $checkCategory;
        } else {
            $cat = new ItemCategory();
            $cat->item = $request->item_category;
            $cat->shop_id = $shopId;

            $cat->save();
        }

        //save to item table
        $this->saveItem($request, $cat);

        $shopItem = new ShopItems();

        $shopItem->name = $request->item_name;
        $shopItem->amount = $request->item_cost;
        $shopItem->available_in_stock = $request->item_stock;
        $shopItem->sold_in = $request->item_sold_in;
        $shopItem->shop_id = $shopId;
        $shopItem->item_category_id = $cat->id;
        $shopItem->desc = $request->desc;
        if ($request->upc) {
            $shopItem->upc = $request->upc;
        }

        // shop item history
        $itemHistoryArr = [];

        $history = [
            'action' => "Item Added",
            "action_by" => lonuxName(),
            "date" => now(),
            "data" => [$shopItem],
        ];

        array_push($itemHistoryArr, $history);

        $history_json = json_encode($itemHistoryArr);

        $shopItem->item_history = $history_json;

        $shopItem->save();

        $is_from = $request->is_from;
        $caller = 'storeInventory';
        $arr = [];
        $slug = 'shop_items_images/' . Str::slug(substr($shopItem->name, 0, 10));

        if ($request->image1) {
            $dir = $slug . '_' . time();
            if ($is_from == "app") {
                $request->image1 = 'data:image/png;base64,' . $request->image1;
            }
            $path = $this->uploadToS3($request->image1, $dir);
            array_push($arr, $path);
        }
        if ($request->image2) {
            $dir = $slug . '_' . time();
            if ($is_from == "app") {
                $request->image2 = 'data:image/png;base64,' . $request->image2;
                // dd($request->image2 );
            }
            $path = $this->uploadToS3($request->image2, $dir);
            array_push($arr, $path);
        }
        if ($request->image3) {
            $dir = $slug . '_' . time();
            if ($is_from == "app") {
                $request->image3 = 'data:image/png;base64,' . $request->image3;
            }
            $path = $this->uploadToS3($request->image3, $dir);
            array_push($arr, $path);
        }
        if ($request->image4) {
            if ($is_from == "app") {
                $request->image4 = 'data:image/png;base64,' . $request->image4;
            }
            $dir = $slug . '_' . time();
            $path = $this->uploadToS3($request->image4, $dir);
            array_push($arr, $path);
        }
        if ($request->image5) {
            $dir = $slug . '_' . time();
            if ($is_from == "app") {
                $request->image5 = 'data:image/png;base64,' . $request->image5;
            }
            $path = $this->uploadToS3($request->image5, $dir);
            array_push($arr, $path);
        }

        $shopItem->images = implode(',', $arr);

        $shopItem->save();

        return $this->send_response(true, "inventory saved successfully", [], 200);
    }

    public function saveItem($request, $cat)
    {
        $item = null;

        if ($request->upc)
        {
            $item = Item::where('upc', $request->upc)->orWhere('item_name', $request->item_name)->first();
        } else {
            $item = Item::where('item_name', $request->item_name)->first();
        }
        
        if (is_null($item)) {
            $item = new Item();
            $item->item_name = $request->item_name;
            $item->item_cost = $request->item_cost;
            $item->item_category_id = $cat->id;
            $is_from = $request->is_from;

            if($request->upc)
            {
                $item->upc = $request->upc;
            }

            $item->save();

            $arr = [];
            $slug = 'items_images/' . Str::slug(substr($item->item_name, 0, 10));

            if ($request->image1) {
                $dir = $slug . '_' . time();
                if ($is_from == "app") {
                    $request->image1 = 'data:image/png;base64,' . $request->image1;
                }

                $path = $this->uploadToS3($request->image1, $dir);
                array_push($arr, $path);
            }
            if ($request->image2) {
                $dir = $slug . '_' . time();
                if ($is_from == "app") {
                    $request->image2 = 'data:image/png;base64,' . $request->image2;
                }
                $path = $this->uploadToS3($request->image2, $dir);
                array_push($arr, $path);
            }
            if ($request->image3) {
                $dir = $slug . '_' . time();
                if ($is_from == "app") {
                    $request->image3 = 'data:image/png;base64,' . $request->image3;
                }
                $path = $this->uploadToS3($request->image3, $dir);
                array_push($arr, $path);
            }
            if ($request->image4) {
                $dir = $slug . '_' . time();
                if ($is_from == "app") {
                    $request->image4 = 'data:image/png;base64,' . $request->image4;
                }
                $path = $this->uploadToS3($request->image4, $dir);
            }
            if ($request->image5) {
                $dir = $slug . '_' . time();
                if ($is_from == "app") {
                    $request->image5 = 'data:image/png;base64,' . $request->image5;
                }
                $path = $this->uploadToS3($request->image5, $dir);
            }

            $item->images = implode(',', $arr);

            $item->save();
        }
        return true;
    }

    public function getLatestInventory()
    {
        $shopItems = ShopItems::where('shop_id', shopId())->latest()->limit(10)->get();

        if (!count($shopItems)) {
            return $this->send_response(false, "no inventry found", [], 404);
        }

        $totalInventoryCount = ShopItems::where('shop_id', shopId())->count();
        $shopItems[0]->totalInventoryCount = $totalInventoryCount;
        $shopItems[0]->shopKey = lonuxManager()->shop()->key;

        return $this->send_response(true, "latest inventries retrieved successfully", $shopItems, 200);
    }

    public function getShopItemsCategory()
    {
        $cats = ItemCategory::where('shop_id', shopId())->get();
        foreach ($cats as $key => $cat) {
            $cat->itemsCount = count($cat->items()->get());
        }
        $totalInventoryCount = ShopItems::where('shop_id', shopId())->count();
        $cats[0]->totalInventoryCount = $totalInventoryCount;

        return $this->send_response(true, "latest inventries retrieved successfully", $cats, 200);
    }

    public function getCategoryItems($id)
    {
        $catItems = ShopItems::where('item_category_id', $id)->get();
        return $this->send_response(true, "category items retrieved successfully", $catItems, 200);
    }

    public function getInventoryItem($id)
    {
        $item = ShopItems::whereId($id)->with('category')->first();
        //    $item->images = $item->images('card');

        return $this->send_response(true, "item retrieved successfully", $item, 200);
    }

    public function itemIsOutOfStock(Request $request)
    {
        $item = ShopItems::findorfail($request->itemId);
        $available_in_stock = $item->available_in_stock;
        $item->sold = $available_in_stock;

        //get history
        $itemHistory = $item->item_history;

        if (!is_null($itemHistory)) {
            $itemHistoryArr = json_decode($itemHistory);
        } else {
            $itemHistory = [];
        }

        $newItem = $item;

        unset($newItem->item_history);

        $history = [
            'action' => "Out of Stock",
            "action_by" => lonuxName(),
            "date" => now(),
            "data" => [$newItem],
        ];

        array_push($itemHistoryArr, $history);

        $history_json = json_encode($itemHistoryArr);

        $item->item_history = $history_json;

        $item->save();

        return $this->send_response(true, "item data updated successfully", [], 200);
    }

    public function deleteItem(Request $request)
    {
        $item = ShopItems::findorfail($request->itemId);



        return $this->send_response(true, "item deleted successfully", [], 200);
    }

    public function updateInventory(Request $request)
    {
        $item = ShopItems::findorfail($request->id);

        $shopId = lonuxManager()->shop()->id;

        $checkCategory = ItemCategory::where('shop_id', $shopId)->where('item', $request->category['item'])->first();
        $cat = null;

        if (!is_null($checkCategory)) {
            $cat = $checkCategory;
        } else {
            $cat = new ItemCategory();
            $cat->item = $request->category['item'];
            $cat->shop_id = $shopId;

            $cat->save();
        }

        $item->name = $request->name;
        $item->amount = $request->amount;
        $item->available_in_stock = $request->available_in_stock;
        $item->sold_in = $request->sold_in;
        $item->sold = $request->sold;
        $item->item_category_id = $cat->id;

        $item->save();

        return $this->send_response(true, "item updated successfully", [], 200);
    }

    public function getBookings()
    {
        $shopId = lonuxManager()->shop()->id;

        $bookings = Booking::where('shop_id', $shopId)->latest()->simplePaginate(2);

        $bookingsCount = Booking::where('shop_id', $shopId)->count();
        $recentBookings = Booking::where('shop_id', $shopId)->where('booking_status', 'Pending')->count();

        if ($bookings) {
            $bookings[0]->totalShopBookings = $bookingsCount;
            $bookings[0]->recentShopBookings = $recentBookings;
        }

        return $this->send_response(true, "bookings retrieved successfully", $bookings, 200);
    }

    public function getBookedItem($id)
    {
        $booking = Booking::find($id);

        return $this->send_response(true, "booking retrieved successfully", $booking, 200);
    }

    public function itemWasPicked(Request $request)
    {
        $itemId = $request->itemId;
        $item = Booking::find($itemId);
        $item->booking_status = 'Picked';
        $item->save();

        $sale = new Sale();
        $sale->shop_id = lonuxManager()->shop()->id;
        $sale->bought_in = $request->boughtIn;
        $sale->buying_channel = $request->buying_channel;
        $sale->pickup_mode = $request->pickup_mode;
        $sale->shop_items_id = $request->shop_items_id;
        $sale->user_id = $request->user_id;
        $sale->quantity = $request->quantity;
        $sale->shop_manager_id = lonuxId();

        $sale->save();

        return $this->send_response(true, "booking data updated successfully", [], 200);
    }

    public function customerDidntCome(Request $request)
    {
        $itemId = $request->itemId;
        $item = Booking::find($itemId);
        $item->booking_status = 'Incomplete';
        $item->save();

        return $this->send_response(true, "booking data updated successfully", [], 200);
    }

    public function searchBooking(Request $request)
    {
        $booking = Booking::where('order_code', $request->orderCode)->first();

        if (is_null($booking)) {
            return $this->send_response(false, "booking not found", [], 404);
        }

        return $this->send_response(true, "booking retrieved successfully", $booking, 200);
    }

    public function getItemByUPC(Request $request)
    {
        $code = $request->code;

        $checkShop = ShopItems::where('upc', $code)->first();
        if (!is_null($checkShop)) {
            $mes = "Shop already has this item. Manage it in manager's dashboard";
            return $this->send_response(false, $mes, [], 403);
        }

        $item = Item::where('upc', $code)->with('category')->first();

        if (is_null($item)) {
            return $this->send_response(false, "item not found", $item, 404);
        }
        $item->images = explode(',', $item->images);
        return $this->send_response(true, "Item retrieved successfully", $item, 200);
    }

    public function changeThisImage(Request $request)
    {
        $model = $request->model;
        $oldImage = $request->oldImage;
        $itemId = $request->itemId;
        $index = $request->index;
        $serviceId = $request->serviceId;

        if ($model === 'item') {
            $modelData = ShopItems::findorfail($itemId);
            $slug = 'shop_items_images/' . Str::slug(substr($modelData->name, 0, 10));
            $imagesField = 'images';
        } else {
            $modelData = OfferedService::findorfail($serviceId);
            $slug = 'shop_service_images/' . Str::slug(substr($modelData->service_name, 0, 10));
            $imagesField = 'service_images';
        }

        try {
            $publicId = $this->getPublicId($oldImage);

            Storage::disk('s3')->delete($publicId);

            $dir = $slug . '_' . time();

            $path = $this->uploadToS3($request->newImage, $dir);

            $imagesArr = explode(',', $modelData->{$imagesField});

            $imagesArr[$index] = $path;

            $modelData->{$imagesField} = implode(',', $imagesArr);
            $modelData->save();
            return $this->send_response(true, "Image updated successfully", $modelData, 200);
        } catch (\Throwable $th) {
            return $this->send_response(false, "Unable to perform action", [], 400);
        }
            
    }

    public function addNewImage(Request $request)
    {
        $model = $request->model;
        $itemId = $request->itemId;
        $serviceId = $request->serviceId;

        if ($model === 'item') {
            $modelData = ShopItems::findorfail($itemId);
            $slug = 'shop_items_images/' . Str::slug(substr($modelData->name, 0, 10));
            $imagesField = 'images';
        } else {
            $modelData = OfferedService::findorfail($serviceId);
            $slug = 'shop_service_images/' . Str::slug(substr($modelData->service_name, 0, 10));
            $imagesField = 'service_images';
        }

        try {
            $dir = $slug . '_' . time();

            $path = $this->uploadToS3($request->image, $dir);

            $imagesArr = $modelData->{$imagesField} == null ? [] : explode(',', $modelData->{$imagesField});
            array_push($imagesArr, $path);

            $modelData->{$imagesField} = implode(',', $imagesArr);
            $modelData->save();

            return $this->send_response(true, "Image added successfully", $modelData, 200);
        } catch (\Throwable $th) {
            //throw $th;
        }

        
    }

    public function deleteImage(Request $request)
    {
        $model = $request->model;
        $itemId = $request->itemId;
        $imageUrl = $request->imageUrl;
        $serviceId = $request->serviceId;

        if ($model === 'item') {
            $modelData = ShopItems::whereId($itemId)->with('category')->first();
            $imagesField = 'images';
        } else {
            $modelData = OfferedService::findorfail($serviceId);
            $imagesField = 'service_images';
        }

        try {
            $publicId = $this->getPublicId($imageUrl);

            Storage::disk('s3')->delete($publicId);

            $imagesArr = explode(',', $modelData->{$imagesField});
            $key = array_search($imageUrl,  $imagesArr);
            unset($imagesArr[$key]);

            if (count($imagesArr)) {
                $modelData->{$imagesField} = implode(',', $imagesArr);
            } else {
                $modelData->{$imagesField} = null;
            }

            $modelData->save();

            return $this->send_response(true, "Image deleted successfully", $modelData, 200);
        } catch (\Throwable $th) {
            return $this->send_response(false, "Unable to perform action", [], 400);
        }

        
    }

    public function getPublicId($url)
    {
        $exploder = env('AWS_BUCKET_URL');

        $oldImageArr = explode($exploder, $url);

        return $oldImageArr[1];
    }

    public function uploadToS3($image, $dir)
    {
        $path = $image->storePublicly(
            $dir,
            's3'
        );
        $path = env('AWS_BUCKET_URL').$path;

        return $path;
    }
}
