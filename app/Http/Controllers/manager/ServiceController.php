<?php

namespace Lonux\Http\Controllers\manager;

use JD\Cloudder\Facades\Cloudder;
use Lonux\Traits\SendResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Lonux\OfferedService;
use Lonux\Http\Controllers\Controller;

class ServiceController extends Controller
{
    use SendResponse;

    public function index()
    {
        $shopServices = lonuxUser()->shop()->services()->latest()->limit(5)->get();

        if (!count($shopServices)) {
            return $this->send_response(false, "no services found", [], 404);
        }

        $totalServicesCount = lonuxUser()->shop()->services()->count();
        $shopServices[0]->totalServicesCount = $totalServicesCount;

        return $this->send_response(true, "latest inventries retrieved successfully", $shopServices, 200);
    }

    public function showAll()
    {
        $shopServices = lonuxUser()->shop()->services()->select('service_name', 'id')->simplePaginate(10);

        if (!count($shopServices)) {
            return $this->send_response(false, "no services found", [], 404);
        }

        $totalServicesCount = lonuxUser()->shop()->services()->count();
        $shopServices[0]->totalServicesCount = $totalServicesCount;

        return $this->send_response(true, "services retrieved successfully", $shopServices, 200);
    }

    public function show($id)
    {
        $shopService = lonuxUser()->shop()->services()->where('id', $id)->first();

        if (is_null($shopService)) {
            return $this->send_response(false, "no service found", [], 404);
        }

        $shopService->salesCount = $shopService->salesCount();

        return $this->send_response(true, "services retrieved successfully", $shopService, 200);
    }

    public function unavailable($id)
    {
        $service = OfferedService::findorfail($id);

        $service->service_availability = false;

        $service->save();

        return $this->send_response(true, "services retrieved successfully", [], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'service_name' => 'required',
            'service_duration' => 'required',
            'service_charge' => 'required',
            'service_availability' => 'required',
            'deliverable' => 'required'
        ]);

        $service = new OfferedService();

        $service->shop_key = lonuxUser()->shop_key;
        $service->service_name = $request->service_name;
        $service->service_duration = $request->service_duration;
        $service->service_charge = $request->service_charge;
        $service->service_availability = (bool)$request->service_availability;
        $service->deliverable = (bool)$request->deliverable;
        $service->service_text = $request->service_text;

        $service->save();

        // take care of images
        $slug = 'shop_service_images/' . Str::slug(substr($service->name, 0, 10));
        $arr = [];

        if ($request->image1) {
            $dir = $slug . '_' . time();
            $uploadedFileUrl = Cloudder::upload($request->image1, $dir)->getResult()["secure_url"];
            array_push($arr, $uploadedFileUrl);
        }
        if ($request->image2) {
            $dir = $slug . '_' . time();
            $uploadedFileUrl = Cloudder::upload($request->image2, $dir)->getResult()["secure_url"];
            array_push($arr, $uploadedFileUrl);
        }
        if ($request->image3) {
            $dir = $slug . '_' . time();
            $uploadedFileUrl = Cloudder::upload($request->image3, $dir)->getResult()["secure_url"];
            array_push($arr, $uploadedFileUrl);
        }

        $service->service_images = implode(',', $arr);

        $service->save();

        $this->send_response(true, 'Service saved successfully', []);
    }

    public function update($id, Request $request)
    {
        $service = OfferedService::findorfail($id);

        $service->update($request->except('service_images', 'id', 'shop_key'));

        return $this->send_response(true, 'Service updated');
    }
}
