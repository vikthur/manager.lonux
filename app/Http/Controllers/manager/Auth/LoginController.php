<?php

namespace Lonux\Http\Controllers\manager\Auth;

use Auth;
use Illuminate\Http\Request;
use Lonux\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers;


    public function __construct()
    {
        $this->middleware('guest:manager')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'phone'   => 'required',
            'password' => 'required'
        ]);

        $phone = $request->phone;
        $password = $request->password;

        if (strlen($phone) == 10) {
            $phone = '234' . $phone;
        }

        if (Auth::guard('manager')->attempt(['phone' => $phone, 'password' => $password])) {
            $user = Auth::guard('manager')->user();
            $user->last_login = now();
            $user->save();
            // return Auth::user();
            return response("/m", 200);
            // dd($auth);
        }
        return response('failed', 403);
    }

    public function showLoginForm()
    {
        return view('manager.login');
    }

    public function logout(Request $request)
    {

        $this->guard('manager')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('/');
    }
}
