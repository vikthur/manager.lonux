<?php

namespace Lonux\Http\Controllers;

use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Lonux\ShopItems;
use Lonux\Sale;
use Lonux\Shop;

class SalesController extends Controller
{
    use SendResponse;
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getShopsSales()
    {
        $shops = lonuxUser()->company->shops()->select('key','shop_name','shop_address')->get();

        foreach ($shops as $key => $shop) {
            $sales = $shop->sales()->whereDate('created_at', date('Y-m-d'))->pluck('quantity')->toArray();
            $count = array_sum($sales);
            
            $shop->todaySales = $count;
        }

        return $this->send_response(true, 'data retrieved successfully', $shops);
    }

    public function getShopSales($key)
    {
        $shop = Shop::getShopByKey($key);
        $salesToday = $shop->sales()->whereDate('created_at', date('Y-m-d'))->pluck('quantity')->toArray();
        $salesAll = $shop->sales()->pluck('quantity')->toArray();

        $todaySales = array_sum($salesToday);
        $salesAll = array_sum($salesAll);

        $shop->todaySales = $todaySales;
        $shop->salesAll = $salesAll;
        
        return $this->send_response(true, "sales retrueved successfully", $shop);
    }

    public function getShopBestSellingItems($key)
    {
        $shop = Shop::getShopByKey($key);
        $sales = [];

        // today
        $today = date('Y-m-d');
        $todaySales = $shop->sales()->whereDate('created_at', $today)->select('quantity', 'shop_items_id')->get()->toArray();

        if(!count($todaySales))
        {
            $sales['today_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($todaySales);
            $sales['today_most_sold'] = $item;
        }

        //yesterday sales
        $yesterday_date = date('Y-m-d',strtotime("-1 days"));
        $yesterdayItemsIds = $shop->sales()->whereDate('created_at', $yesterday_date)->select('quantity','shop_items_id')->get()->toArray();
        if(!count($yesterdayItemsIds))
        {
            $sales['yesterday_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($yesterdayItemsIds);
            $sales['yesterday_most_sold'] = $item;

            // dd("yesterday",$item);
        }

        // last week date range
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);

        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);

        $lastWeekItemsIds = $shop->sales()->whereBetween('created_at', [$start_week, $end_week])->select('quantity','shop_items_id')->get()->toArray();
        if(!count($lastWeekItemsIds))
        {
            $sales['last_week_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($lastWeekItemsIds);
            $sales['last_week_most_sold'] = $item;
        }

        // this month date range
        $start_month = date("Y-m-d",strtotime("first day of this month"));
        $end_month = date("Y-m-d", strtotime("last day of this month"));

        $thisMonthItemsIds = $shop->sales()->whereBetween('created_at',[$start_month, $end_month])->select('quantity','shop_items_id')->get()->toArray();
        if(!count($thisMonthItemsIds))
        {
            $sales['this_month_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($thisMonthItemsIds);
            $sales['this_month_most_sold'] = $item;
        }

        // all time best selling item
        $allTimeItemsIds = $shop->sales()->select('quantity','shop_items_id')->get()->toArray();
        if(!count($allTimeItemsIds)){
            $sales['all_time_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($allTimeItemsIds);
            $sales['all_time_most_sold'] = $item;
        }
        return $this->send_response(true, "sales retreived successfully", $sales);
    }

    public function getBestSellingItems()
    {
        $sales = [];
        $shopsIds = lonuxUser()->company->shops()->pluck('id')->toArray();

        // today sales
        $today = date('Y-m-d');
        $todayItemsIds = Sale::whereIn('shop_id', $shopsIds)->whereDate('created_at', $today)->select('quantity','shop_items_id')->get()->toArray();
        // dd($todayItemsIds);
        if(!count($todayItemsIds))
        {
            $sales['today_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($todayItemsIds);
            $sales['today_most_sold'] = $item;
        }
        

        //yesterday sales
        $yesterday_date = date('Y-m-d',strtotime("-1 days"));
        $yesterdayItemsIds = Sale::whereIn('shop_id', $shopsIds)->whereDate('created_at', $yesterday_date)->select('quantity','shop_items_id')->get()->toArray();
        if(!count($yesterdayItemsIds))
        {
            $sales['yesterday_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($yesterdayItemsIds);
            $sales['yesterday_most_sold'] = $item;

            // dd("yesterday",$item);
        }

        // last week date range
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);

        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);

        $lastWeekItemsIds = Sale::whereIn('shop_id', $shopsIds)->whereBetween('created_at', [$start_week, $end_week])->select('quantity','shop_items_id')->get()->toArray();
        if(!count($lastWeekItemsIds))
        {
            $sales['last_week_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($lastWeekItemsIds);
            $sales['last_week_most_sold'] = $item;
        }

        // this month date range
        $start_month = date("Y-m-d",strtotime("first day of this month"));
        $end_month = date("Y-m-d", strtotime("last day of this month"));

        $thisMonthItemsIds = Sale::whereIn('shop_id', $shopsIds)->whereBetween('created_at',[$start_month, $end_month])->select('quantity','shop_items_id')->get()->toArray();
        if(!count($thisMonthItemsIds))
        {
            $sales['this_month_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($thisMonthItemsIds);
            $sales['this_month_most_sold'] = $item;
        }

        // all time best selling item
        $allTimeItemsIds = Sale::whereIn('shop_id', $shopsIds)->select('quantity','shop_items_id')->get()->toArray();
        if(!count($allTimeItemsIds)){
            $sales['all_time_most_sold'] = "none";
        }else{
            $item = $this->generateBestSellingItem($allTimeItemsIds);
            $sales['all_time_most_sold'] = $item;
        }
        return $this->send_response(true, "sales retreived successfully", $sales);
    }

    public function getChartsData(Request $request)
    {
        
        $this->validate($request, [
            'start' => 'required',
            'end' => 'required',
        ]);

        $start = $request->start;
        $end = $request->end;

        $chartData = $this->generateChartData($start, $end);

        return $this->send_response(true, 'chart data generated successfully', $chartData);
    }

    public function getShopSevenDaysChartData($key)
    {
        $today = date("Y-m-d");
        $sevenDaysAgo = date("Y-m-d", strtotime('-7 days'));

        $chartData = $this->generateChartData($sevenDaysAgo, $today, $key);

        return $this->send_response(true, 'chart data generated successfully', $chartData);
    }

    public function getSevenDaysChartData()
    {
        $today = date("Y-m-d");
        $sevenDaysAgo = date("Y-m-d", strtotime('-7 days'));

        $chartData = $this->generateChartData($sevenDaysAgo, $today);

        return $this->send_response(true, 'chart data generated successfully', $chartData);
    }

    public function generateBestSellingItem($arrData)
    {
        $newArr = [];
        foreach($arrData as $key => $value)
        {
            $newArrVal = $value['quantity'];
            $newArrKey = $value['shop_items_id'];

            if(array_key_exists($newArrKey, $newArr))
            {
                $newArr[$newArrKey] += $newArrVal;
            }else{
                $newArr[$newArrKey] = $newArrVal;
            }
        }

        $max = max($newArr);
        $key = array_search($max, $newArr);
        
        $item = ShopItems::where("id", $key)->with('category:id,item')->first();
        $item->count = $max;

        return $item;
    }

    public function generateChartData($start, $end, $key = null)
    {
        if($key == null)
        {
            $shopsIds = lonuxUser()->company->shops()->pluck('id')->toArray();

            $sales = Sale::whereIn('shop_id', $shopsIds)->whereBetween('created_at',[$start, $end])->select('quantity', 'created_at')->get()->toArray();
        }else{
            
            $shop = Shop::getShopByKey($key);

            $sales = $shop->sales;
        }

        $newArr = [];
        foreach($sales as $key => $value)
        {
            $newArrVal = $value['quantity'];
            $newArrKey = date("Y-m-d",strtotime($value['created_at']));

            if(array_key_exists($newArrKey, $newArr))
            {
                $newArr[$newArrKey] += $newArrVal;
            }else{
                $newArr[$newArrKey] = $newArrVal;
            }
        }
        $datesArr = [];
        $salesArr = [];

        foreach ($newArr as $key => $value){
            array_push($salesArr, $value);
            array_push($datesArr, $key);
        }

        $chartData = [$salesArr, $datesArr];

        return $chartData;
    }

    public function ViewShopSales($key)
    {
        $shop = Shop::getShopByKey($key);

        $sales = $shop->sales()->with('shopItem')->simplePaginate(10);

        return $this->send_response(true, "sales retrieved successfully", $sales);
    }

}
