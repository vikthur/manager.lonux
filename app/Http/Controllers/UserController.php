<?php

namespace Lonux\Http\Controllers;

use Lonux\ShopStats;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Lonux\Traits\UserActivityTrait;

class UserController extends Controller
{
    use SendResponse, UserActivityTrait;

    public function saveUserActivity(Request $request)
    {
        $this->validate($request, [
            'activity_type' => 'required',
            'user_agent' => 'required',
            'client_location' => 'required',
            'activity_data' => 'required',
        ]);

        $message = $this->saveActivity($request);

        return $this->send_response(true, $message);
    }

    public function saveShopSearchAppearance(Request $request)
    {
        dd($request->all());
        $this->validate($request, [
            'search_term' => 'required',
            'user_agent' => 'required',
            'shopStats' =>  'required|array'
        ]);

        $st =  $request->search_term;
        $ua =  $request->user_agent;
        $ci =  $this->getUserIp();
        $userId =  lonuxUser() ? lonuxId() : null;

        foreach($request->shopStats as $shopStat)
        {
            $stat = new ShopStats();
            $stat->search_term = $st;
            $stat->user_agent = $ua;
            $stat->client_ip = $ci;
            $stat->user_id = $userId;
            $stat->shop_id = $shopStat['shopId'];
            $stat->search_app_pos = $shopStat['position'];

            $stat->save();
        }

        return $this->send_response(true, "Shop search Appearance saved successfully");
    }

    public function getStats()
    {
        $stats = [
            'purchases' => count(lonuxUser()->purchases),
            'bookings' => count(lonuxUser()->bookings),
        ];

        return $this->send_response(true, "statistics retrieved successfully", $stats);
    }

    public function getBookings()
    {
        $bookings = lonuxUser()->bookings;

        return $this->send_response(true, "User bookings retrieved successfully", $bookings);
    }

    public function getActivities()
    {
        $activities = lonuxUser()->activities;

        return $this->send_response(true, "User activities retrieved successfully", $activities);

    }
}
