<?php

namespace Lonux\Http\Controllers;

use Lonux\User;
use Lonux\Phone;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class PhoneNumberVerification extends Controller
{
  use SendResponse;
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('guest');
  }

  public function index(){
    return view('auth.phone');
  }

  public function changePassword(Request $request)
  {
    $phoneNumber = "234".$request->phoneNumber;
    $pass = $request->password;
    $pass2 = $request->passwordConfirm;

    if(strlen($pass) < 8){
      return $this->send_response(false, "Password cannot be lessthan eight (8) letters", [], 403);
    }

    if($pass !== $pass2){
      return $this->send_response(false, "passwords do not match", [], 403);
    }

    $newPassword = Hash::make($pass);
    
    $user = User::where('phone', $phoneNumber)->first();

    if (is_null($user)) {
      return $this->send_response(false, 'Phone Number Not Registered', [], 404);
    }

    $user->password = $newPassword;

    $user->save();

    return $this->send_response(true, "Password Changed Successfully", [], 200);

  }

  public function sendCode($phoneNumber)
  {
    /* Get credentials from .env */
    $token = getenv("TWILIO_AUTH_TOKEN");
    $twilio_sid = getenv("TWILIO_SID");
    $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
    $twilio = new Client($twilio_sid, $token);

    try {
      $twilio->verify->v2->services($twilio_verify_sid)
      ->verifications
      ->create($phoneNumber, "sms");
    } catch (\Throwable $th) {
      if(get_class($th) == 'Twilio\Exceptions\RestException')
      {
        return $this->send_response(false, 'Invalid Phone Number', [], 403);
      }
    }
  }

  public function verifyCode($code, $phoneNumber)
  {
    /* Get credentials from .env */
    $token = getenv("TWILIO_AUTH_TOKEN");
    $twilio_sid = getenv("TWILIO_SID");
    $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
    $twilio = new Client($twilio_sid, $token);

    $verification = $twilio->verify->v2->services($twilio_verify_sid)
    ->verificationChecks
    ->create($code, array('to' => $phoneNumber));

    return $verification;
  }

  public function phoneVerification(Request $request)
  {
    $phoneNumber = "+234".$request->phoneNumber;

    if (is_null($request->code)) {

      if($request->action == 'reset_code')
      {
        $this->sendCode($phoneNumber);
        return $this->send_response(true, "Code Sent Successfully", [], 200);
      }

      $check = User::where('phone', $phoneNumber)->first();
      if (!is_null($check)) {
        return $this->send_response(false, 'Phone Number Already Exists', [], 403);
      }

      //send code here
      $this->sendCode($phoneNumber);


      $phone = Phone::where('phone', $phoneNumber)->first();

      if(is_null($phone)){
        $phone = new Phone();

        $phone->phone = $phoneNumber;
        $phone->save();
      }
      return $this->send_response(true, "Code Sent Successfully", [], 200);

    }
    else {

      $phone = Phone::where('phone', $phoneNumber)->first();

      if (is_null($phone)) {
        return $this->send_response(false, "Phone Number Not Found", [], 404);
      }

      $verification = $this->verifyCode($request->code, $phoneNumber);


      if ($verification->valid) {

        if($request->action == 'reset_code')
        {
          return $this->send_response(true, "Verification Success", [], 200);
        }

        $phone->is_used = true;
        $phone->save();

        return $this->send_response(true, "Verification Success", [], 200);
      }else{
        return $this->send_response(false, "Invalid Code", [], 403);
      }
    }

  }

  public function changeNumber($phone){

    $phone = Phone::where('phone', $phone)->where('is_used', false)->first();

    if (is_null($phone)) {
      return $this->send_response(true, "Phone Number Change Success", [], 200);
    }else{
      $phone->delete();
      return $this->send_response(true, "Phone Number Change Success", [], 200);
    }

  }
}
