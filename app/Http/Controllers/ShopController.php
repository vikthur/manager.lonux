<?php

namespace Lonux\Http\Controllers;

use Lonux\Shop;
use Lonux\ShopType;
use Lonux\ShopItems;
use Lonux\ItemCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Auth;

class ShopController extends Controller
{
	use SendResponse;

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'shop_name'		=>	['required','string'],
    		'shop_address'	=>	['required','string'],
    	]);

    	$shop = new Shop();

    	$shopTypeArray = [];

    	$shop->shop_name =  $request->shop_name;
        $shop->shop_address = $request->shop_address;
        $shop->lat = $request->lat;
        $shop->lng = $request->lng;
    	$shop->shop_service_id = $request->shop_service;
    	$shop->shop_sells_in = $request->shop_sell_in;
    	$shop->shop_work_days = implode(',', $request->shop_work_days);
    	$shop->shop_open_at = $request->shop_open_at;
    	$shop->shop_close_at = $request->shop_close_at;
    	$shop->is_up = false;
        $shop->key = (string)Str::uuid();
    	foreach ($request->shop_types as $shopType) {
    		array_push($shopTypeArray, $shopType['id']);
    		$shopTypeModel = ShopType::find($shopType['id']);
    		$shopTypeModel->increment('count');
    	}
    	if ($request->newShopAdded) {
    		$newShopTypes = explode(',', (string)$request->newShopAdded);
    		foreach ($newShopTypes as $newShopType) {
    			$createShop = ShopType::create(['name'=>$newShopType,'count'=>1, 'shop_service_id' => $request->shop_service]);
    			array_push($shopTypeArray, $createShop->id);
    		}
    	}
    	$shop->shop_type_ids = implode(',', $shopTypeArray);

    	$shop->company_id = lonuxUser()->company->id;

    	$shop->user_id = lonuxId();


    	$shop->save();

        $shop->update([
            'contact_code' => 'LX35'.$this->formatId($shop->id)
        ]);

        $shop->shopService->increment('count');

        return $this->send_response(true, 'shop created successfully', $shop);

    }

    public function showAll()
    {
    	$shops = lonuxUser()->company->shops;

        return $this->send_response(true, 'shops retreived successfully', $shops);
    }

    public function show($key)
    {
        $shop = Shop::getShopByKey($key);
        if (is_null($shop)) {
            return response("notFound", 404);
        }else{
            $shop = Shop::where('key',$key)->with('itemsCategory','shopService','sales','items','managers')->first();
            $shop->image = $shop->image('thumb');
            $shop->shop_rating = $shop->shopRating();

            return $this->send_response(true, "shop retrieved successfully", $shop);
        }
    }

    public function storeNewItems(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'itemsCategory'     =>  'required|string',
            'shopKey'            =>  'required',
            'soldIn'            =>  'required'
        ]);

        // $shop = new Shop();
        $shopId = Shop::getShopByKey($request->shopKey)->id;

        $checkCategory = ItemCategory::where('shop_id', $shopId)->where('item', $request->itemsCategory)->first();
        $cat = null;

        if (!is_null($checkCategory)) {
            $cat = $checkCategory;
        }else{
            $cat = new ItemCategory();

            $cat->item = $request->itemsCategory;
            $cat->shop_id = $shopId;

            $cat->save();
        }


        $items = array_filter($request->items, function($item){
            return $item['itemName'] != null && $item['itemAmount'] != null && $item['itemNumber'] != null ;
        });

        foreach ($items as $key => $item) {
            ShopItems::create([
                'name'                  =>      $item['itemName'],
                'amount'                =>      $item['itemAmount'],
                'available_in_stock'    =>      $item['itemAmount'],
                'sold_in'               =>      $request->soldIn,
                'shop_id'               =>      $shopId,
                'item_category_id'      =>      $cat->id,
            ]);
        }

        return $this->send_response(true, 'items saved successfully');
    }

    public function getCategoryItems($id)
    {
        $cat = ItemCategory::where('id',$id)->first();
        $shopItems = ShopItems::where('item_category_id', $id)->orderBy('created_at','desc')->simplePaginate(10);
        foreach ($shopItems as $key => $shopItem) {
            $shopItem->salesCount = $shopItem->salesCount();
        }
        return $this->send_response(true, "items retrieved successfully", $shopItems);
    }

    public function getSearchResults(Request $request)
    {
        $catId = $request->catId;
        $searchTerm = $request->searchTerm;

        if(is_null($searchTerm)){
            return $this->getCategoryItems($catId);
        }

        $items = ShopItems::where('item_category_id', $catId)->where('name', 'LIKE', "%{$searchTerm}%")->simplePaginate(10);

        return $this->send_response(true, 'search success', $items);
    }

    public function updateSingleShopItem(Request $request)
    {
        $item = ShopItems::findorFail($request->id);

        $item->name = $request->name;
        $item->amount = $request->amount;
        $item->sold_in = $request->sold_in;
        $item->available_in_stock = $request->available_in_stock;

        $item->save();

        return $this->send_response(true, "items updated successfully", $cat);
    }

    public function updateShopStatus(Request $request, $key)
    {
        $shop = Shop::where('key', $key)->first();

        $shop->is_up = (int)$request->status;

        $shop->save();

        return $this->send_response(true, "shop status updated successfully");
    }

    public function getShopStatus($key)
    {
        $shop = Shop::where('key', $key)->first();
        $status = $shop->is_up;
        return $this->send_response(true, "shop status retrieved successfully", $status);
    }

    public function getWhatThisShopSell($key){
        $shopTypeArr = explode(',', Shop::getShopByKey($key)->shop_type_ids);

        $shopType = ShopType::whereIn('id', $shopTypeArr)->get();

        return $this->send_response(true, "items retrieved successfully", $shopType);
    }

    public function UpdateShop(Request $request)
    {
        $shop = Shop::find($request->id);

        $shop->shop_name = $request->shop_name;
        $shop->shop_address = $request->shop_address;
        $shop->shop_sells_in = $request->shop_sells_in;
        $shop->shop_work_days = implode(',', $request->shop_work_days);
        $shop->shop_open_at = $request->shop_open_at;
        $shop->shop_close_at = $request->shop_close_at;
        if ($request->shopTypes) {
            $shopTypesArr = [];
            foreach ($request->shop_typesArr as $key => $shopType) {
                $shopId = ShopType::where('name', $shopType)->first()->id;
                array_push($shopTypesArr, $shopId);
            }
            $shop->shop_type_ids = implode(',', $shopTypesArr);
        }

        $shop->save();

        return $this->send_response(true, "shop updated successfully");
    }

    public function saveShopImage(Request $request)
    {
        // dd($request->image);
        $shop = Shop::find($request->id);
        $this->validate($request, [
            'image' => 'required|image'
        ]);
        if ($shop->media->first()) {
            $shop->clearMediaCollection('shop_image');
        }
        $shop
           ->addMedia($request->image)
           ->toMediaCollection('shop_image');

       $shop->save();

       return $this->send_response(true, "image saved successfully");
    }

    public function changeShopTime(Request $request)
    {
        $shop = lonuxManager()->shop()->update([
            'shop_open_at' => $request->open_at,
            'shop_close_at' => $request->close_at,
        ]);

        return $this->send_response(true, "shop modified successfully");
    }

    public function formatId($id)
    {
        $len = strlen($id);
        $expected = 4;
        if ($len < $expected) {
            $diff = $expected - $len;
            for ($i=0; $i < $diff ; $i++) {
                $id = '0'.$id;
            }
        }
        return $id;

    }

}
