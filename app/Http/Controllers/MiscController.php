<?php

namespace Lonux\Http\Controllers;

use Lonux\LGA;
use Lonux\State;
use Lonux\Rating;
use Lonux\Company;
use Lonux\Country;
use Lonux\ShopType;
use Lonux\ShopItems;
use Lonux\ShopService;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;

class MiscController extends Controller
{
    use SendResponse;

    public function get_lonux_countries(){

    	$countries = Country::all();

    	return response($countries, 200);

    }

    public function get_lonux_states($country){

    	$states = State::where('country', $country)->get();

    	return response($states, 200);

    }

    public function get_lonux_lgas($state){

    	$lgas = LGA::where('state', $state)->get();

    	return response($lgas, 200);

    }

    public function checkBusinessName($name)
    {
        $check = Company::where('name', $name)->first();

        if (is_null($check)) {
            return $this->send_response(true, 'available');
        }else{
            return $this->send_response(true, 'used');
        }
    }

    public function getShopTypes($id)
    {
        $shop_types = ShopType::where('shop_service_id', $id)->get();

        return $this->send_response(true, 'shop types retrieved', $shop_types);
    }

    public function getShopServices()
    {
        $shop_services = ShopService::all();

        return $this->send_response(true, 'shop types retreived', $shop_services);
    }

    public function saveRating(Request $request)
    {
        $this->validate($request, [
            'shop_id' => 'required',
            'rating' => 'required'
        ]); 

        $rating = new Rating();

        $rating->user_id = lonuxUser() ? lonuxId() : null;
        $rating->shop_id = $request->shop_id;
        $rating->rating = $request->rating;
        $rating->desc = $request->desc;

        $rating->save();

        $response = [
            'data' => '',
            'message' => 'ok',
        ];

        return $this->send_response(true, 'rating saved');
    }
}
