<?php

namespace Lonux\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.home');
    }

    public function finish_setup(Request $request)
    {
        $user = lonuxUser();
        $user->account_type_id = $request->selected;
        $user->country = $request->country;
        $user->state = $request->state;
        $user->LGA = $request->lga;
        $user->has_setup = true;

        $user->save();

        return response('ok', 200);
    }
}
