<?php

namespace Lonux\Http\Controllers\Auth;

use Auth;
use Lonux\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * determine which credencial user is signing in with.
     *
     * @return void
     */
    public function username(){

        $username = filter_var(request()->input('email') , FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        return $username;
    }

    /**
     * login the user.
     *
     * @return void
     */
    public function login()
    {
        $login = Auth::attempt([
            $this->username() => request()->input('email'),
            'password'=> request()->input('password')], true);

        if ($login) {
            return response($this->redirectTo, 200);
        }else{
            return response('failed', 403);
        }

    }

}
