<?php

namespace Lonux\Http\Controllers\api;

use Auth;
use Lonux\User;
use Lonux\Phone;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Illuminate\Support\Facades\Hash;
use Lonux\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class AuthController extends Controller
{
    use SendResponse;

    // users login
    public function login(Request $request)
    {
        $login = Auth::attempt([
            'phone' => $request->phone,
            'password'=> $request->password], true);

            // dd($login);

        if ($login) {

            $token = lonuxUser()->createToken('auth_token')->plainTextToken;

            $data = [
                'user'  => lonuxUser(),
                'token' => $token,
            ];
            return $this->send_response(true, "login suceess", $data);
        }else{
            return $this->send_response(false, "login failed", [], 401);
        }
    }

    // managers login
    public function mLogin(Request $request)
    {
        $login = Auth::guard('manager')->attempt([
            'phone' => $request->phone,
            'password' => $request->password,
        ]);

        if($login) {
            lonuxManager()->update([
                'last_login' => now(),
            ]);

            // Revoke all tokens...
            if (lonuxManager()->tokens) 
            {
                lonuxManager()->tokens()->delete();
                // dd(lonuxManager()->tokens);
            }

            $token = lonuxManager()->createToken('m_auth_token')->plainTextToken;

            $data = [
                'user' => lonuxManager(),
                'shop' => lonuxManager()->shop(),
                'token' => $token
            ];

            return $this->send_response(true, 'login success', $data);
        }else{
            return $this->send_response(false, "login failed", [], 401);
        }
    }

    public function register(Request $request)
    {
        $phone = Phone::where("phone", $request->phone)->first();
        if (is_null($phone)) {
            $request->phone == null;
        }

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'unique:users'],
            'address' => ['required','string', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            'lat' => ['required'],
            'lng' => ['required'],
        ]);

        $emailName = str_replace(" ", "",ucwords($request->name));
        $email = $emailName."@lonux.com.ng";
        $email = $this->UniqueEmail($email, $emailName);

        $user = User::create([
            'email' => $email,
            'name'  => $request->name,
            'phone' =>$request->phone,
            'address'   => $request->address,
            'password' => Hash::make($request->password),
            'lat' => $request->lat,
            'lng' => $request->lng,
        ]);

        if ($user)
        {
            Auth::guard()->login($user);

            $token = $user->createToken('auth_token')->plainTextToken;

            $response = [
                'data' => [
                    'user'  => $user,
                    'token' => $token,
                ],
                'message' => 'ok'
            ];

            return response($response, 200);
        }
    }

    public function UniqueEmail($email, $name){
        
        $user = User::where("email",$email)->first();
        if (!is_null($user)) {
            $num = rand(10, 10000);
            $email = $email.$num."@lonux.com.ng";
            $this->UniqueEmail($email);
        }else{
            return $email;
        }
        // $user = $this->checkEmail($email);
        // $email = null;
        // while (!is_null($user)) {
        //   $newEmail = $this->generateNewEmail($name);
        //   $email = $newEmail;
        //   $user = $this->checkEmail($newEmail);
        // }

        // return $email;
    }

    public function generateNewEmail($name)
    {
      $num = rand(10, 10000);
      $email = $name.$num."@lonux.com.ng";

      return $email;
    }

    public function checkEmail($email)
    {
      $user = User::where("email", $email)->first();

      return $user;
    }
}
