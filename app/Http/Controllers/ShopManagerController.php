<?php

namespace Lonux\Http\Controllers;

use Auth;
use Lonux\Shop;
use Lonux\ShopManager;
use Illuminate\Http\Request;
use Lonux\Traits\SendResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class ShopManagerController extends Controller
{
    use SendResponse;

    public function index(Request $request)
    {
        $managers = lonuxUser()->company->managers;
        foreach ($managers as $key => $manager) {
            $manager->shop = $manager->shop();
        }

        return $this->send_response(true, "managers retrieved",$managers);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        // check number of shop managers
        // dd(lonuxUser()->company->id);
        $this->validator($request->all())->validate();

        $shop = Shop::getShopByKey($request->shop_key);
        $managersCount = count($shop->managers);
        if($managersCount < 3){
            $emailName = str_replace(" ", "",ucwords($request->name));
            $email = $emailName."@lonux.com.ng";
            $email = $this->UniqueEmail($email, $emailName);
            ShopManager::create([
                'shop_key' => $request->shop_key,
                'company_id' => lonuxUser()->company->id,
                'name' => $request->name,
                'email' => $email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
            ]);
            return $this->send_response(true, "Manager Created Successfully");
        }else{
            return $this->send_response(false, "Maximum number of Shop Managers reached for this shop",[],401);
        }
    }


    public function show($key)
    {
        $managers = ShopManager::whereShopKey($key)->get();

        return $this->send_response(true, "retreived successfully", $managers);
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        ShopManager::find($id)->delete();

        return "ok";
    }

    public function changeManagerData(Request $request)
    {
        $manager = ShopManager::find($request->id);

        if ($request->type == 'key') {
            $manager->shop_key = $request->key;
        }

        if ($request->type == 'status') {
            $manager->is_active = (int)$request->status;
        }

        if ($request->type == 'password') {
            $manager->password = Hash::make($request->password);
        }

        if($request->type == 'name'){
            $manager->name = $request->name;
         }
        
        $manager->save();

        return $this->send_response(true, "successful operation");
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'phone' => 'required|unique:shop_managers',
            'shop_key' => 'required',
            'password' => 'required|string|min:6',
        ]);
    }

    public function UniqueEmail($email, $name){
        $user = $this->checkEmail($email);
        while (!is_null($user)) {
          $newEmail = $this->generateNewEmail($name);
          $email = $newEmail;
          $user = $this->checkEmail($newEmail, $name);
        }

        return $email;
    }

    public function generateNewEmail($name)
    {
      $num = rand(10, 10000);
      $email = $name.$num."@lonux.com.ng";

      return $email;
    }

    public function checkEmail($email)
    {
      $user = ShopManager::where("email",$email)->first();

      return $user;
    }
}
