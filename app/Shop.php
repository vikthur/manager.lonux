<?php

namespace Lonux;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\File;

class Shop extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function user()
    {
        return $this->belongsTo('Lonux\User');
    }

    public function company()
    {
        return $this->belongsTo('Lonux\Company');
    }

    public function itemsCategory()
    {
        return $this->hasMany('Lonux\ItemCategory');
    }

    public function shopService()
    {
        return $this->hasOne('Lonux\ShopService', 'id', 'shop_service_id');
    }

    public function managers()
    {
        return $this->hasMany('Lonux\ShopManager', 'shop_key', 'key');
    }

    public function sales()
    {
        return $this->hasMany('Lonux\Sale');
    }

    public function items()
    {
        return $this->hasMany('Lonux\ShopItems');
    }

    public function services()
    {
        return $this->hasMany('Lonux\OfferedService', 'shop_key', 'key');
    }

    public function isServiceShop()
    {
        return $this->shop_service_id == 2 ? true : false;
    }

    public function isOpenSalesShop()
    {
        return $this->shop_service_id == 1 ? true : false;
    }

    //custome help methods
    public static function getShopByKey($key)
    {
        return self::where('key', $key)->first();
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('shop_image')
            // ->withResponsiveImages()
            ->acceptsMimeTypes(['image/jpeg', 'image/png', 'image/jpg', 'image/*'])
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('large')
                    ->width(600)
                    ->height(394)
                    ->sharpen(10);

                $this
                    ->addMediaConversion('card')
                    ->width(215)
                    ->height(190)
                    ->sharpen(10);

                $this
                    ->addMediaConversion('thumb')
                    ->width(150)
                    ->height(150)
                    ->sharpen(10);
            });
    }

    public function image($res)
    {
        if ($this->media->first()) {
            return $this->media->first()->getFullUrl($res);
        } else {
            $baseUrl = \Illuminate\Support\Facades\URL::to('/');
            return $baseUrl . Storage::url('/defaults/lonux-shop-medium.jpg');
        }
    }

    public function rating()
    {
        return $this->hasMany('Lonux\Rating');
    }

    public function shopRating()
    {
        $ratingArr = $this->rating->toArray();
        if (count($ratingArr)) {
            $totalRating = 0;

            foreach ($ratingArr as $key => $rating) {
                $totalRating += $rating['rating'];
            }

            $averageRating = $totalRating / count($ratingArr);
            $res = [
                'rating' => $averageRating,
                'count' => count($ratingArr)
            ];
            return $res;
        } else {
            return $res = [
                'rating' => 0,
                'count' => 0,
            ];
        }
    }
}
