<?php

namespace Lonux;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function shop()
    {
    	return $this->belongsTo('Lonux\Shop');
    }

    public function shopItem()
    {
        return $this->belongsTo('Lonux\ShopItems', 'shop_items_id', 'id');
    }
}
