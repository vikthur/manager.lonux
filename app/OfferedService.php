<?php

namespace Lonux;

use Illuminate\Database\Eloquent\Model;

class OfferedService extends Model
{
    // none editable fileds
    protected $guarded = ['id', 'shop_key'];


    public function sales()
    {
        return $this->hasMany('Lonux\Sale');
    }

    public function salesCount()
    {
        $salesArr = $this->sales->toArray();
        // dd($salesArr);
        if (count($salesArr)) {
            $totalSales = 0;
            foreach ($salesArr as $key => $value) {
                $totalSales += $value['quantity'];
            }
            return $totalSales;
        }

        return 0;
    }
}
