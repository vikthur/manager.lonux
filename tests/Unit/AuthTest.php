<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AuthTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_that_user_can_send_code()
    {
        $res = $this->get('/auth/phone');

        $res->assertStatus(200);
    }
}
