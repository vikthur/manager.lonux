<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_that_send_code_route_is_okay()
    {
        $response = $this->get('/auth/phone');

        $response->assertStatus(200);
    }

    public function test_that_user_can_send_code()
    {
        $response = $this->json('POST', '/auth/phone', ['phoneNumber' => '9020354898', 'code' => '34454']);

        $response->assertStatus(200);
    }
}
